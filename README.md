# better_spotify_genres

Create a Spotify playlist from saved albums filtering by genres and years.  Genres are retrieved
from Discogs because Spotify genres are spotty.

## Use

    python better_spotify_genres.py --genre GENRE {GENRE ...} --years STARTYEAR ENDYEAR --save

For all options execute

    python better_spotify_genres.py --help

## Required Python packages

    pip install spotipy python3_discogs_client argparse

The home of spotipy is https://github.com/spotipy-dev/spotipy.

The home of python3_discogs_client is https://github.com/joalla/discogs_client.

## Registering a Spotify app

Follow [these steps](https://developer.spotify.com/documentation/general/guides/authorization/app-settings/) to
create an app and get a client ID, a client secret, and redirect URI (an arbitrary URL).

Set shell variables to these values:

    export SPOTIFY_CLIENT_ID="some string"
    export SPOTIFY_CLIENT_SECRET="some string"
    export SPOTIFY_REDIRECT_URI="http://localhost:8227/"

## Obtaining a Discogs personal token

On the Discogs web page, login and click on your icon at the top right, then select Settings, Developers, Generate New Token.  Set shell variable to the token string.

    export DISCOGS_TOKEN="some string"





