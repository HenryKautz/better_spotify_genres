# Script that creates a Spotify playlist based on saved albums filtered by genre
# Get genre information from Discogs since Spotify genre coverage is poor

# Get id, secret, and redirect_url from Spotify developer page
# Get personal access token from Discogs -> My Avatar -> Settings -> Developer

import discogs_client
import spotipy
from spotipy.oauth2 import SpotifyOAuth
import random
import argparse  
import sys
import time
import string
import os
import re

# Fix garbled titles and artists
REWRITE = [ ('Un Temoin Dans La Ville','Un Temoin Dans La Ville' ),
        ('Cool Man Cool', 'Cool Man Cool'),
        ('Deep: The Classic 1980 New York Studio Session Remixed', 'Deep'),
        ('Lil Johnson & Barrel House Annie', 'Lil Johnson & Barrel House Annie – Complete Recorded Works In Chronological Order'),
        ('Japanese Jazz Spectacle Vol.I','Japanese Jazz Spectacle Vol. I'),
        ('1917-36','The Complete Original Dixieland Jazz Band'),
        ('Masters of the Boogie Woogie Piano','Boogie Woogie Piano B-1005'),
        ("Steve Angrum or George Lewis with Kid Sheik's Band","Steve Angrum George Lewis with Kid Sheik's Band"),
        ("Mr. Gavitt: Calypsos of Costa Rica","Calypsos - Afro-Limonese Music Of Costa Rica"),
        ("Fantastic Frank Strozier - Plus","Fantastic Frank Strozier"),
    ]

RATE_LIMIT = 70  # Spotify calls allowed in 30 seconds.  Rate limit for Spotify is approximately 180 per minute.

# Manual slow down is only needed for Spotipy, the Discogs Client
# automatically handles rate limiting
webcalls = 0
def slowdown():
    global webcalls
    webcalls += 1
    if webcalls % RATE_LIMIT == 0:
        print(f"Completed {RATE_LIMIT} web calls, taking a nap")
        time.sleep(35)
        print('Waking up')

def rewrite(s:str)->str:
    for (pattern,new) in REWRITE:
        if re.search(pattern, s, re.IGNORECASE):
            return new
    return s

# Use L and D as needed to explicitly typecheck responses 
def L(obj)->list:
    assert(isinstance(obj,list))
    return obj

def D(obj)->dict:
    assert(isinstance(obj,dict))
    return obj

def any_in(items:list[str], s:str):
    if len(items)==0:
        return True
    for item in items:
        if item in s:
            return True
    return False

def any_none(good:list[str], bad:list[str], s:str, exact:bool=False):
    okay = False
    if len(good)==0:
        okay = True
    else:
        for item in good:
            if exact:
                item = '#' + item + '#'
            if item in s:
                okay = True
                break
    if not okay:
        return False
    for item in bad:
        if exact:
            item = '#' + item + '#'
        if item in s:
           return False
    return True

spotify_client_id =os.environ.get('SPOTIFY_CLIENT_ID')
spotify_client_secret =os.environ.get('SPOTIFY_CLIENT_SECRET')
spotify_redirect_uri = os.environ.get('SPOTIFY_REDIRECT_URI')
discogs_token = os.environ.get('DISCOGS_TOKEN')



def make_genre_playlist(
        spotify_client_id:str,
        spotify_client_secret:str,
        spotify_redirect_uri:str,
        discogs_token:str,
        playlist_name:str = '',
        genre:list[str] = [],
        notgenre:list[str] = [],
        exact_match:bool = False,
        startyear:int = 0,
        endyear:int = 1000000,
        in_datafile:str='',
        out_datafile:str=''
        ):

    # Create Discogs client
    d = discogs_client.Client('better_spotify_genres/0.1', user_token=discogs_token)

    # Create Spotify client
    scope = "user-library-read playlist-modify-public playlist-modify-private"
    sp = spotipy.Spotify(
        auth_manager=
        SpotifyOAuth(scope=scope,
                    client_id=spotify_client_id,
                    client_secret=spotify_client_secret,
                    redirect_uri=spotify_redirect_uri))

    # Get saved albums
    offset = 0
    done = False
    albums = []

    while not done: 
        slowdown()
        response = D(sp.current_user_saved_albums(limit=50, offset=offset))
        items = response['items']
        if len(items)<50:
            done = True
        albums += items
        offset += len(items)
        print(f"Downloaded {offset} albums...")


    print(f"Found {len(albums)} saved albums")

    # Process each album
    num_matching_albums = 0
    all_tracks = []
    for item in albums:
        try:
            album = (item)['album']
            album_title = album['name']
            primary_artist_name = album['artists'][0]['name']

            # Look up the album genres on Discogs
            #   Covert title and artist to ascii
            #   Remove material in parenthesis from title
            #   Drop substrings in () or [] in title
            #   Drop artists named "various artists"
            # 1. match artist and title, type=master
            # 2. Drop type=master
            # 3. general search of artist + ' ' + title
            # 4. general search of title
            # If multiple results returned: search through first 5 to 
            # find lowest year > 1900 to use as date.

            #print(f"Searching Discogs for {primary_artist_name} -- {album_title}") 

            album_title = album_title.encode('ascii','ignore').decode('ascii')
            primary_artist_name = primary_artist_name.encode('ascii','ignore').decode('ascii')
            album_title = re.sub('\\(.*\\)', '', album_title)
            album_title = re.sub('\\[.*\\]', '', album_title)

            album_title = rewrite(album_title)
            primary_artist_name = rewrite(primary_artist_name)
            
            album_genres = []
            album_title = re.sub('[([]].*[])]','',album_title).strip()
            if re.search('artists', primary_artist_name, flags=re.I):
                primary_artist_name = ''
            
            #print(f"primary_artist_name = {primary_artist_name}") 
            #print(f"album_title = {album_title}") 

            results = d.search(title=album_title, type='master', artist=primary_artist_name)
            if (results.count==0):
                results = d.search(title=album_title, artist=primary_artist_name)
                if results.count==0:
                    results = d.search(primary_artist_name + ' ' + album_title)
                    if results.count==0:
                        results = d.search(album_title)
                        if results.count==0:
                            # Try shortening title
                            album_title = ' '.join((album_title.split())[:10])
                            if results.count==0:
                                print(f"Skipping {primary_artist_name} -- {album_title}, no match in Discogs")
                                continue
            
            if len(results.page(0)) == 0:
                print(f"Skipping {primary_artist_name} -- {album_title}, Discogs error")
                continue

            # print(f"Count Discogs matches = {results.count}") #DEBUG
            # print(f"title = {results[0].title}")

            # Get genres including styles
            album_genres = L(getattr(results[0], 'genres', []))
            if not album_genres:
                album_genres = []

            # print(f"genres = {album_genres}")
            
            album_styles = L(getattr(results[0], 'styles', []))
            if not album_styles:
                album_styles = []

            # print(f"styles = {album_styles}")

            album_genres += album_styles
            if len(album_genres) == []:
                print(f"Warning: No genres found for {primary_artist_name}, {album_title}")
            
            genres_string = '#' + ('#'.join(album_genres)).casefold() + '#'
            
            # Get earliest year
            lowest_album_year = 3000
            assert(isinstance(results.count, int))
            for i in range(0,min(results.count, 5)):
                try:  # Sometimes the count is larger than the number of results
                    if not hasattr(results[i], 'year'):
                        continue
                    album_year = int(results[i].year)
                    if album_year > 0 and album_year < lowest_album_year:
                        lowest_album_year = album_year
                except IndexError:
                    pass

            # If album matches genre and year range, then add its tracks to all_tracks
            if ( any_none(genre, notgenre, genres_string, exact_match) and
                startyear <= lowest_album_year and lowest_album_year <= endyear):
                    num_matching_albums += 1
                    tracks = D(album['tracks'])
                    tracklist = L(tracks['items'])
                    print(f"\nAdding {len(tracklist)} tracks from {primary_artist_name}, {album_title} ({lowest_album_year}): {genres_string}")
                    for track in tracklist:
                        trackid = D(track)['id']
                        all_tracks.append(trackid)
            else:
                print('.',end='',flush=True)
        except (AssertionError, AttributeError, IndexError, KeyError, NameError, RuntimeError, ValueError ) as e:
            print(f"Unknown Discogs error, had to bail on {item['album']['name']}")
            continue

    print(f"Number of {genre} albums is {num_matching_albums}")
    print(f"Total number of tracks is {len(all_tracks)}")

    if playlist_name:

        # Get user id - not the same as client id
        profile = D(sp.me())
        my_id = profile['id']

        # Create playlist
        slowdown()
        playlist = D(sp.user_playlist_create(user=my_id, name=playlist_name))
        playlist_id = playlist['id']

        print(f"Created playlist {playlist_name}")

        # Shuffle tracks
        random.shuffle(all_tracks)

        # Add all tracks to playlist
        remaining = len(all_tracks)
        if remaining > 10000:
            print(f"Bailing out, Spotify limits playlists to 10,000 tracks", file=sys.stderr)
            exit(1)
        offset = 0
        while remaining>0:
            chunksize = min(50,remaining)
            chunk = all_tracks[offset:offset+chunksize]

            slowdown()
            sp.playlist_add_items(playlist_id=playlist_id, items=chunk)
            remaining -= chunksize
            print(f"Uploaded {chunksize} tracks, {remaining} remain")

        print(f"Added tracks to playlist {playlist_name}")


def main():
    parser = argparse.ArgumentParser(description="Create playlist from saved albums filtered by genre")
    parser.add_argument('--save', '-s', action='store_true', help='Save playlist to Spotify, otherwise is a dry run')
    parser.add_argument('--name', '-n', type=str, default='', help='Name of playlist, if omitted name is created from genre')
    parser.add_argument('--genre', '-g', action='extend', default=[], type=str, nargs='+', help='One or more genres to include')
    parser.add_argument('--except', '-x', dest='notgenre', metavar='GENRE', action='extend', default=[], type=str, nargs='+', help='One or more genres to exclude')
    parser.add_argument('--exact', '-e', action='store_true', help='Match genres exactly, otherwise substring match permitted')
    parser.add_argument('--years', '-y', type=int, metavar=('START_YEAR','END_YEAR'), default=[0,1000000], nargs=2, help='Only match albums between pair of years inclusive')
    parser.add_argument('--read', '-r', type=str, default='', metavar='FILE', help='Export data to file')
    parser.add_argument('--write', '-w', type=str, default='', metavar='FILE', help='Import data from file')

    args = parser.parse_args()
    save:str = args.save
    genre:list[str] = args.genre
    notgenre:list[str] = args.notgenre
    name:str = args.name 

    if not name:
        if genre:
            name = 'My ' + string.capwords(' '.join(genre)) + ' Albums'
        else:
            name = 'All My Albums'
        if args.years[0]>0:
            name += ' ' + str(args.years[0]) + '-' + str(args.years[1])
    if not save:
        name = ''
    genre = [ s.casefold() for s in genre ]
    notgenre = [ s.casefold() for s in notgenre]
    
    make_genre_playlist(
        str(os.environ.get('SPOTIFY_CLIENT_ID')),
        str(os.environ.get('SPOTIFY_CLIENT_SECRET')),
        str(os.environ.get('SPOTIFY_REDIRECT_URI')),
        str(os.environ.get('DISCOGS_TOKEN')),
        playlist_name=name,
        genre=genre,
        notgenre=notgenre,
        exact_match=args.exact,
        startyear=args.years[0],
        endyear=args.years[1],
        in_datafile=args.read,
        out_datafile=args.write
        )

if __name__ == '__main__':
    main()

